/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.renu.mycomponnent;

import java.util.ArrayList;

/**
 *
 * @author ray
 */
public class Product {
   private int id; 
   private String name;
   private double price;
   private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }
   public static ArrayList<Product> genProductList(){
       ArrayList<Product>list = new ArrayList<>();
       list.add(new Product(1,"เอสเพรสโซ่ 1",250,"7s (1).jpeg"));
       list.add(new Product(2,"เอสเพรสโซ่ 2",230,"8s (1).jpeg"));
       list.add(new Product(3,"เอสเพรสโซ่ 3",200,"9s (1).jpeg"));
       list.add(new Product(4,"อเมริกาโน่ 1",180,"4a (1).jpeg"));
       list.add(new Product(5,"อเมริกาโน่ 2",200,"5a (1).jpeg"));
       list.add(new Product(6,"อเมริกาโน่ 3",220,"6a (1).jpeg"));
       list.add(new Product(7,"คาบูชิโน่ 1",230,"1 (1).jpeg"));
       list.add(new Product(8,"คาบูชิโน่ 2",220,"2 (1).jpeg"));
       list.add(new Product(9,"คาบูชิโน่ 3",210,"3 (1).jpeg"));
       return list;
   }
}
